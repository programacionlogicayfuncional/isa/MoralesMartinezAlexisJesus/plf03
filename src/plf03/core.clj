(ns plf03.core)

(defn función-comp-1
  []
  (let [f (fn [x] (map dec x))
        g (fn [x] (filter pos? x))
        z (comp g f)]
    (z #{-1 2 4 5 0})))

(defn función-comp-2
  []
  (let [f (fn [x] (vector x))
        g (fn [x] (reverse x))
        z (comp f g)]
    (z "hello")))

(defn función-comp-3
  []
  (let [f (fn [x] (+ 50 x))
        g (fn [x] (inc x))
        h (fn [x] (* 3 x))
        z (comp f g h)]
    (z 1000)))

(defn función-comp-4
  []
  (let [f (fn [x] (repeat 2 x))
        g (fn [x] (first x))
        h (fn [x] (filter pos? x))
        z (comp f g h)]
    (z '(2 4 5 6))))

(defn función-comp-5
  []
  (let [f (fn [x] (* 10 x))
        g (fn [x] (+ 3 x))
        h (fn [x] (inc x))
        z (comp g f h)]
    (z 30)))

(defn función-comp-6
  []
  (let [f (fn [x] (range x))
        g (fn [x] (first x))
        h (fn [x] (pos? x))
        z (comp h g f)]
    (z 9)))

(defn función-comp-7
  []
  (let [f (fn [x] (range x))
        g (fn [x] (first x))
        h (fn [x] (+ 10 x))
        z (comp h g f)]
    (z 5)))

(defn función-comp-8
  []
  (let [f (fn [x] (double? x))
        g (fn [x] (vector x))
        z (comp g f)]
    (z 20)))

(defn función-comp-9
  []
  (let [f (fn [x] (take 3 x))
        g (fn [x] (str x))
        h (fn [x] (first x))
        z (comp g h f)]
    (z [10 20 30 60 80])))

(defn función-comp-10
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [xs] (map-entry? xs))
        z (comp f g)]
    (z '(2 3 4 5))))

(defn función-comp-11
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (+ 3 x))
        z (comp f g)]
    (z 10)))

(defn función-comp-12
  []
  (let [f (fn [x] (+ 5 x))
        g (fn [x] (- 2 x))
        z (comp f g)]
    (z 3)))

(defn función-comp-13
  []
  (let [f (fn [x] (filter pos? x))
        g (fn [x] (map inc x))
        h (fn [x] (filter even? x))
        z (comp f g h)]
    (z #{5 -4 8 4 7 -10 12 -20 10 -3})))

(defn función-comp-14
  []
  (let [f (fn [x] (into [] (remove pos? x)))
        g (fn [x] (reverse x))
        h (fn [x] (map inc x))
        z (comp f g h)]
    (z [1 2 -5 4 -9 -6 -8])))


(defn función-comp-15
  []
  (let [f (fn [x] (list? x))
        g (fn [x] (map-entry? x))
        z (comp f g)]
    (z '(1 3 6 8 10))))


(defn función-comp-16
  []
  (let [f (fn [x] (second x))
        g (fn [x] (reverse x))
        z (comp f g)]
    (z '("a" 2 7 20 30 "c"))))

(defn función-comp-17
  []
  (let [f (fn [x] (keyword x))
        g (fn [x] (str x))
        z (comp f g)]
    (z [1 2])))


(defn función-comp-18
  []
  (let [f (fn [x] (= 4 x))
        g (fn [x] (not x))
        z (comp f g)]
    (z 10)))

(defn función-comp-19
  []
  (let [f (fn [x] (not x))
        g (fn [x] (zero? x))
        z (comp f g)]
    (z 8)))

(defn función-comp-20
  []
  (let [f (fn [x] (str x))
        g (fn [xs] (double xs))
        z (comp f g)]
    (z (+ 1 2 3 4))))

(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)


;fin

;complement
;

(defn función-complement-1
  []
  (let [f (fn [x] (pos-int? x))
        z (complement f)]
    (z 100M)))

(defn función-complement-2
  []
  (let [f (fn [x] (rational? x))
        z (complement f)]
    (z (+ 2 4 8/2 3/8))))

(defn función-complement-3
  []
  (let [f (fn [x] (boolean? x))
        z (complement f)]
    (z false)))

(defn función-complement-4
  []
  (let [f (fn [x] (float? x))
        z (complement f)]
    (z 10.04)))

(defn función-complement-5
  []
  (let [f (fn [x] (pos? x))
        z (complement f)]
    (z 5)))

(defn función-complement-6
  []
  (let [f (fn [x] (odd? x))
        z (complement f)]
    (z 54)))

(defn función-complement-7
  []
  (let [f (fn [x] (even? x))
        z (complement f)]
    (z 205)))

(defn función-complement-8
  []
  (let [f (fn [x] (associative? x))
        z (complement f)]
    (z '(2 5 6 7 8))))

(defn función-complement-9
  []
  (let [f (fn [x] (char? x))
        z (complement f)]
    (z [:a :b :c])))

(defn función-complement-10
  []
  (let [f (fn [x] (keyword? x))
        z (complement f)]
    (z [:a :b :c :d :e])))

(defn función-complement-11
  []
  (let [f (fn [xs] (map even? xs))
        z (complement f)]
    (z [1 2 3 4 7 8])))

(defn función-complement-12
  []
  (let [f (fn [x] (float? x))
        z (complement f)]
    (z 50)))

(defn función-complement-13
  []
  (let [f (fn [x] (associative? x))
        z (complement f)]
    (z "Bienvenidos a plf")))

(defn función-complement-14
  []
  (let [f (fn [x] (boolean? x))
        z (complement f)]
    (z (new Boolean "false"))))

(defn función-complement-15
  []
  (let [f (fn [x] (coll? x))
        z (complement f)]
    (z ["hola" "buenas" "noches"])))

(defn función-complement-16
  []
  (let [f (fn [x] (map-entry? x))
        z (complement f)]
    (z (last '(3 6 9 12)))))

(defn función-complement-17
  []
  (let [f (fn [x] (map? x))
        z (complement f)]
    (z (dec 150))))

(defn función-complement-18
  []
  (let [f (fn [x] (nat-int? x))
        z (complement f)]
    (z (* 1 20))))

(defn función-complement-19
  []
  (let [f (fn [x] (number? x))
        z (complement f)]
    (z 2400)))

(defn función-complement-20
  []
  (let [f (fn [x] (count x))
        z (complement f)]
    (z [2 4 5 6 3])))

(función-complement-1)
(función-complement-2)
(función-complement-4)
(función-complement-3)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)

;fin complement

;inicio constantly
(defn función-constantly-1
  []
  (let [f 11.5
        z (constantly f)]
    (z  number? 10)))

(defn función-constantly-2
  []
  (let [f []
        z (constantly f)]
    (z char? 20)))

(defn función-constantly-3
  []
  (let [f "IMPORTANTE"
        z (constantly f)]
    (z double? 2)))

(defn función-constantly-4
  []
  (let [f {:a 2 :b 4 :c 6}
        z (constantly f)]
    (z false 4)))

(defn función-constantly-5
  []
  (let [f  100
        z (constantly f)]
    (z  int? 10)))

(defn función-constantly-6
  []
  (let [f  "PLF03 PLF04"
        z (constantly f)]
    (z  rational? '("1" "2" "3" "4"))))

(defn función-constantly-7
  []
  (let [f  true
        z (constantly f)]
    (z  '([1 2 3 4 444 1]))))

(defn función-constantly-8
  []
  (let [x  [true "hola" false false]
        f (constantly x)]
    (f  "Bienvenido plf03")))

(defn función-constantly-9
  []
  (let [x  '("hola " \a 1 false)
        f (constantly x)]
    (f boolean?  false)))

(defn función-constantly-10
  []
  (let [x  #{2 4 6 8}
        f (constantly x)]
    (f (* 2 4))))


(defn función-constantly-11
  []
  (let [f '(1 2 3)
        z (constantly f)]
    (z 5)))

(defn función-constantly-12
  []
  (let [f "CHIVAS"
        z (constantly f)]
    (z [1 2 5 6])))

(defn función-constantly-13
  []
  (let [f '(2 4 6 8)
        z (constantly f)]
    (z  {3 6 8 12})))

(defn función-constantly-14
  []
  (let [f "MATEMATICAS"
        z (constantly f)]
    (z [:a :b :c :d])))

(defn función-constantly-15
  []
  (let [f [\a \b \c]
        z (constantly f)]
    (z {:a 2 :b 4})))

(defn función-constantly-16
  []
  (let [f {:a "Bienvenido " :b "   " :c "   "}
        z (constantly f)]
    (z  #{24 6 8 10})))

(defn función-constantly-17
  []
  (let [f (dec 350)
        z (constantly f)]
    (z  [1 2 3 4 5 6 7 8])))

(defn función-constantly-18
  []
  (let [f (inc 190)
        z (constantly f)]
    (z  #{2 4 6 8 9})))

(defn función-constantly-19
  []
  (let [f (string? "plf03")
        z (constantly f)]
    (z (iterate inc 5))))

(defn función-constantly-20
  []
  (let [f (last '(2 4 6 8 10))
        z (constantly f)]
    (z 5)))

(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)

;fin constantly

;inicio every-pred

(defn función-every-pred-1
  []
  (let [f (fn [x] (group-by neg? x))
        g (fn [x] (number? (f x)))
        z (every-pred f g)]
    (z [-2 3 -3 4 -4 1 -1 2])))

(defn función-every-pred-2
  []
  (let [f (fn [x] (filter float? x))
        g (fn [x] (string? (first x)))
        z (every-pred f g)]
    (z #{"a" "e" "i" "o" "u"})))


(defn función-every-pred-3
  []
  (let [f (fn [x] (indexed? x))
        g (fn [x] (list x))
        z (every-pred f g)]
    (z {:a 2 :b 4})))


(defn función-every-pred-4
  []
  (let [f (fn [x] (+ 5 x))
        g (fn [x] (- 1 x))
        h (fn [x] (int? x))
        z (every-pred f g h)]
    (z 50)))


(defn función-every-pred-5
  []
  (let [f (fn [x] (* x 3.6))
        g (fn [x] (/ 3 x))
        h (fn [x] (pos? x))
        z (every-pred h f g)]
    (z 5)))

(defn función-every-pred-6
  []
  (let [f (fn [x] (filter pos? x))
        g (fn [x] (filter even? x))
        h (fn [x] (reverse x))
        z (every-pred h f g)]
    (z #{2 3 4 -4 -5 0 -1 10 -2})))

(defn función-every-pred-7
  []
  (let [f (fn [x] (first x))
        g (fn [x] (count (filter string? x)))
        z (every-pred f g)]
    (z '(1 3 7 5 "hola"))))

(defn función-every-pred-8
  []
  (let [f (fn [x] (boolean? x))
        g (fn [x] (char? x))
        h (fn [x] (string? x))
        z (every-pred f g h)]
    (z (new Boolean "true"))))


(defn función-every-pred-9
  []
  (let [f (fn [x] (float? x))
        g (fn [x] (< 5 x))
        z (every-pred f g)]
    (z (+ 1/2 2.5 4 5))))

(defn función-every-pred-10
  []
  (let [f (fn [x] (map-entry? x))
        g (fn [x] (integer? x))
        z (every-pred g f)]
    (z (first '(2 4 6 8 10)))))

(defn función-every-pred-11
  []
  (let [f (fn [x] (number? x))
        g (fn [x] (neg? x))
        h (fn [x] (pos? (last x)))
        z (every-pred f g h)]
    (z  #{3 5 6 7 8})))


(defn función-every-pred-12
  []
  (let [f (fn [x] (first x))
        g (fn [x] (count (filterv neg? x)))
        z (every-pred g f)]
    (z [10 -20 30 -40 50])))

(defn función-every-pred-13
  []
  (let [f (fn [x] (seqable? x))
        g (fn [x] (+ 12 x))
        h (fn [x] (- 5 x))
        z (every-pred f g h)]
    (z 4)))


(defn función-every-pred-14
  []
  (let [f (fn [x] (int? x))
        g (fn [x] (key x))
        h (fn [x] (keyword x))
        z (every-pred f g h)]
    (z {:a :b :c :d})))

(defn función-every-pred-15
  []
  (let [f (fn [x] (filter pos? x))
        g (fn [x] (float? x))
        h (fn [x] (+ 8 (first x)))
        z (every-pred g h f)]
    (z 3 -1 -2 3 5)))

(defn función-every-pred-16
  []
  (let [f (fn [x] (filter int? x))
        g (fn [x] (count x))
        z (every-pred g f)]
    (z [2 \a \b 1 \c 4])))


(defn función-every-pred-17
  []
  (let [f (fn [x] (filter true? x))
        g (fn [x] (complement x))
        h (fn [x] (+ 5 x))
        z (every-pred h g f)]
    (z 2 4 6 7)))

(defn función-every-pred-18
  []
  (let [f (fn [x] (filter boolean? x))
        g (fn [x] (complement x))
        z (every-pred g f)]
    (z [\a \b \c true])))

(defn función-every-pred-19
  []
  (let [f (fn [x] (float? x))
        g (fn [x] (pos? x))
        h (fn [x] (* 2 (first x)))
        z (every-pred g h f)]
    (z (- 3.3 1 3 5 4.5 6))))


(defn función-every-pred-20
  []
  (let [f (fn [x] (map? x))
        g (fn [x] (number? x))
        h (fn [x] (dec x))
        z (every-pred f g h)]
    (z (inc 33))))



(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)

;fin every-pred

;inicio fnil

(defn función-fnil-1
  []
  (let [f (fn [x y] (- x y))
        z (fnil f nil nil)]
    (z 18 87)))


(defn función-fnil-2
  []
  (let [f (fn [x y] (< x y))
        z (fnil f nil)]
    (z 13 4)))


(defn función-fnil-3
  []
  (let [f (fn [x y] (and (* x y) (+ x y) (- x y)))
        z (fnil f 3)]
    (z nil 5)))

(defn función-fnil-4
  []
  (let [f (fn [x y] (== (+ x y) (* x y)))
        z (fnil f nil)]
    (z 1 1)))

(defn función-fnil-5
    []
  (let [f (fn [x] (str "plf01 " x))
        z (fnil f "plf03")]
    [(z "Estas son")]))

(defn función-fnil-6
  []
  (let [f (fn  [x] (str "Buenas noches " x))
        z (fnil f "")]
    (z 2)))

(defn función-fnil-7
  []
  (let [f (fn [x] (x))
        z (fnil inc f)]
    (z 7)))

(defn función-fnil-8
  []
  (let [f (fn [x] (int? x))
        g (fn [x] (+ 356 x))
        z (fnil f g)]
    (z 5)))

(defn función-fnil-9
  []
  (let [f (fn [x] (last x))
        g (fn [x] (* 3 x))
        z (fnil f g)]
    (z '(3 4 5 6 7))))

(defn función-fnil-10
  []
  (let [f (fn [x] (first x))
        g (fn [x] (+ 34 x))
        z (fnil f g)]
    (z [3 1 15 5 6])))

(defn función-fnil-11
  []
  (let [f (fn [x] (map-entry? x))
        g (fn [x] (- 1 x))
        z (fnil f g)]
    (z '(22 5 6 7 8))))

(defn función-fnil-12
  []
  (let [f (fn [x] (dec x))
        g (fn [x] (+ 7 x))
        z (fnil f g)]
    (z 3)))

(defn función-fnil-13
  []
  (let [f (fn [x y] (== (inc x) (dec y)))
        z (fnil f nil)]
    (z 6 7)))

(defn función-fnil-14
  []
  (let [f (fn [x] (take 2 x))
        z (fnil f (range 2 6))]
    (z nil)))

(defn función-fnil-15
  []
  (let [f (fn [x y] (number? (/ x y)))
        z (fnil f nil)]
    (z 10 5)))

(defn función-fnil-16
  []
  (let [f (fn  [x] (str "Muñeca" x))
        z (fnil f "")]
    (z "Es mi novia")))

(defn función-fnil-17
  []
  (let [f (fn  [x] (dec x))
        z (fnil f nil)]
    (z 5)))

(defn función-fnil-18
  []
  (let [f (fn  [x] (inc x))
        z (fnil f nil)]
    (z 2456)))

(defn función-fnil-19
  []
  (let [f (fn  [x y] (float? (- x y)))
        z (fnil f nil)]
    (z 3 1)))
(función-fnil-19)

(defn función-fnil-20
  []
  (let [f (fn  [x y] (* 4 x y))
        z (fnil f nil)]
    (z 5 5)))


(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)


;fin fnil

;inicio juxt
(defn función-juxt-1
  []
  (let [f (fn [x] (first x))
        g (fn [x] (count x))
        z (juxt f g)]
    (z "alexis")))

(defn función-juxt-2
  []
  (let [f (fn [x] (take 3 x))
        g (fn [x] (drop 4 x))
        z (juxt f g)]
    (z [1 2 3 4 5 6])))

(defn función-juxt-3
  []
  (let [f (fn [x] (first x))
        g (fn [x] (str x))
        z (juxt g f)]
    (z [4 5 6 7])))

(defn función-juxt-4
  []
  (let [f (fn [x] (map inc x))
        g (fn [x] (filter odd? x))
        z (juxt g f)]
    (z #{3 4 5 6})))

(defn función-juxt-5
  []
  (let [f (fn [x] (count x))
        g (fn [x] (first x))
        z (juxt g f)]
    (z [1 3 5 6 7])))

(defn función-juxt-6
  []
  (let [f (fn [x] (+ 3 x))
        g (fn [x] (* 1 x))
        h (fn [x] (dec  x))
        z (juxt h g f)]
    (z 10)))

(defn función-juxt-7
  []
  (let [f (fn [x] (filter char? x))
        g (fn [x] (count x))
        h (fn [x] (vector x))
        z (juxt h g f)]
    (z '(1 \a "hola" \b 2 "tres" \x))))

(defn función-juxt-8
  []
  (let [f (fn [x] (last x))
        g (fn [x] (list x))
        h (fn [x] (conj [1000] x))
        z (juxt h g f)]
    (z [4 2 1 6 8 10 15 33 100 5])))

(defn función-juxt-9
  []
  (let [f (fn [x] (take 5 x))
        g (fn [x] (reverse x))
        z (juxt g f)]
    (z "Hola me Llamo Roberto")))


(defn función-juxt-10
  []
  (let [f (fn [x] (range x))
        g (fn [x] (+ 40 x))
        h (fn [x] (int? x))
        z (juxt f h g)]
    (z 12)))

(defn función-juxt-11
  []
  (let [f (fn [x] (map key x))
        g (fn [x] (map val x))
        z (juxt f g)]
    (z {:a 2 :b 4 :c 6 :d 8})))

(defn función-juxt-12
  []
  (let [f (fn [x] (count x))
        g (fn [x] (:a :b x))
        z (juxt f g)]
    (z {:a 2 :b 4 :c 6 :d 8})))

(defn función-juxt-13
  []
  (let [f (fn [x] (sort x))
        g (fn [x] (conj [100 200] x))
        z (juxt f g)]
    (z "abcdaabccc")))

(defn función-juxt-14
  []
  (let [f (fn [x xy xz] (max x xy xz))
        g (fn [x xy xz] (min x xy xz))
        h (fn [x xy xz] (+ x xy xz))
        z (juxt f g h)]
    (z  10 4 12)))

(defn función-juxt-15
  []
  (let [f (fn [x] (remove true? x))
        g (fn [x] (last x))
        h (fn [x] (filter boolean? x))
        z (juxt g f h)]
    (z '(1 -2 true 2 -1 false 3 7 false 0 true true))))

(defn función-juxt-16
  []
  (let [f (fn [x] (vector? x))
        g (fn [x] (list? x))
        h (fn [x] (map? x))
        z (juxt g f h)]
    (z '("juan" "pepe" "josue" "karla"))))

(defn función-juxt-17
  []
  (let [f (fn [x] (take-last 2 x))
        g (fn [x] (update x 1 inc))
        h (fn [x] (count x))
        z (juxt g f h)]
    (z [24 25 26 27])))

(defn función-juxt-18
  []
  (let [f (fn [x] (drop-last x))
        g (fn [x] (remove pos? x))
        z (juxt g f)]
    (z #{1 0 -2 4 5 -10 -30 8 10 -1})))

(defn función-juxt-19
  []
  (let [f (fn [x] (sort-by count x))
        g (fn [x] (string? x))
        z (juxt f g)]
    (z ["abc" "a" "b" "cd"])))

(defn función-juxt-20
  []
  (let [f (fn [x xs] (+ x xs))
        g (fn [x xs] (- x xs))
        h (fn [x xs] (/ x xs))
        z (juxt g f h)]
    (z 10 14)))


(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)

;fin juxt
;
;inicio partial

(defn función-partial-1
  []
  (let [f (fn [x y z] (hash-set x y z 5))
        z (partial f)]
    (z 2 4 6)))


(defn función-partial-2
  []
  (let [f (fn [x y z] (list x y z \d))
        z (partial f)]
    (z 2 4 6)))

(defn función-partial-3
  []
  (let [f (fn [x] (/ 1000 x))
        z (partial f)]
    (z 5)))
(defn función-partial-5
  []
  (let [f (fn [x] (filter char? x))
        z (partial f)]
    (z "Mi ranchito")))
(defn función-partial-4
  []
  (let [f (fn [x] (sort-by char? x))
        z (partial f)]
    (z "My name is Roberto")))

(defn función-partial-6
  []
  (let [f (fn [x] (sequential? x))
        z (partial f)]
    (z "Hola buenos dias")))

(defn función-partial-7
  []
  (let [f (fn [x] (filterv even? x))
        z (partial f)]
    (z [1 2 3 4 5 6 7 8])))

(defn función-partial-8
  []
  (let [f (fn [x] (filterv neg? x))
        z (partial f)]
    (z [1 2 3 4 5 -6 7 -8])))

(defn función-partial-9
  []
  (let [f (fn [x] (+ 10 x))
        z (partial f)]
    (z 15)))

(defn función-partial-10
  []
  (let [f (fn [x] (* 10 x))
        z (partial f)]
    (z 25)))

(defn función-partial-11
  []
  (let [f (fn [x] (- 10 x))
        z (partial f)]
    (z 30)))

(defn función-partial-12
  []
  (let [f (fn [x] (/ 10 x))
        z (partial f)]
    (z 35)))

(defn función-partial-13
  []
  (let [f (fn [x] (hash-map x 1000))
        z (partial f)]
    (z #{2 4 5 6})))

(defn función-partial-14
  []
  (let [f (fn [x] (take-while boolean? x))
        z (partial f)]
    (z [false true -10 5.5])))

(defn función-partial-15
  []
  (let [f (fn [x] (group-by char? x))
        z (partial f)]
    (z '("añex" "3455"))))

(defn función-partial-16
  []
  (let [f (fn [x] (take-while char? x))
        z (partial f)]
    (z "Feliz Navidad")))


(defn función-partial-17
  []
  (let [f (fn [x] (remove char? x))
        z (partial f)]
    (z '(\a \b \c \d))))

(defn función-partial-18
  []
  (let [f (fn [x] (filter pos? x))
        z (partial f)]
    (z [-10 20 -30 40 50])))

(defn función-partial-19
  []
  (let [f (fn [x] (take-while number? x))
        z (partial f)]
    (z [false true  5.5 -5 2 3])))

(defn función-partial-20
  []
  (let [f (fn [x] (partition-by odd? x))
        z (partial f)]
    (z [1 1 1 2 2 3 3])))

(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)


;inicia some-fn
(defn función-some-fn-1
  []
  (let [f (fn [x] (group-by char? x))
        g (fn [x] (even?  x))
        h (fn [x] (pos?  x))
        z (some-fn f h g)]
    (z [\a \b -3 \c 4 -4])))



(defn función-some-fn-2
  []
  (let [f (fn [x] (group-by neg? x))
        g (fn [x] (filterv pos? x))
        h (fn [x] (even? x))
        z (some-fn f h g)]
    (z [3 -3 4 -4 0 -1 2 -2])))


(defn función-some-fn-3
  []
  (let [f (fn [x] (group-by number? x))
        g (fn [x] (first true x))
        h (fn [x] (+ 4 (first x)))
        z (some-fn f h g)]
    (z '(2 4 5 6 true false))))


(defn función-some-fn-4
  []
  (let [f (fn [x] (group-by string? x))
        g (fn [x] (last number? x))
        h (fn [x] (even? x))
        z (some-fn f h g)]
    (z [3 4 5 6 7 8 -9])))

(defn función-some-fn-5
  []
  (let [f (fn [x] (group-by odd? x))
        g (fn [x] (vector? x))
        h (fn [x] (count x))
        z (some-fn f h g)]
    (z [-2 3 1 4 -5 -6 2 40])))


(defn función-some-fn-6
  []
  (let [f (fn [x] (take-while number? x))
        g (fn [x] (first char? x))
        h (fn [x] (pos? x))
        z (some-fn f h g)]
    (z '(1 \a 4 "holis"))))


(defn función-some-fn-7
  []
  (let [f (fn [x] (remove int? x))
        g (fn [x] (last * 4 x))
        h (fn [x] (count x))
        z (some-fn h f g)]
    (z [2 4 5 5.5 6 2.2 1])))


(defn función-some-fn-8
  []
  (let [f (fn [x] (remove boolean? x))
        g (fn [x] (count x))
        h (fn [x] (complement (first x)))
        z (some-fn g h f)]
    (z '(false true 4 5 6))))


(defn función-some-fn-9
  []
  (let [f (fn [x] (drop-while pos? x))
        g (fn [x] (filterv neg? x))
        h (fn [x] (neg? x))
        z (some-fn f h g)]
    (z (vector -1 5 6 7 -2 -5))))



(defn función-some-fn-10
  []
  (let [f (fn [x] (drop-while neg? x))
        g (fn [x] (map inc x))
        h (fn [x] (char? x))
        z (some-fn f h g)]
    (z '(-1 -2 -3 1 2 3 6))))

(defn función-some-fn-11
  []
  (let [f (fn [x] (filter pos? x))
        g (fn [x] (dec x))
        h (fn [x] (* 5 (last x)))
        z (some-fn f h g)]
    (z [-5 22 -3 -4 -8 7 3 1])))

(defn función-some-fn-12
  []
  (let [f (fn [x] (filter neg? x))
        g (fn [x] (take 3 x))
        h (fn [x] (float? x))
        z (some-fn f h g)]
    (z '(5.5 -10 3 -12 -5 0))))

(defn función-some-fn-13
  []
  (let [f (fn [x] (filterv string? x))
        g (fn [x] (str first x))
        h (fn [x] (reverse x))
        z (some-fn f h g)]
    (z '(1 "4" 4 "3" 3 "1"))))


(defn función-some-fn-14
  []
  (let [f (fn [x] (filter number? x))
        g (fn [x] (last boolean? x))
        h (fn [x] (complement x))
        z (some-fn f g h)]
    (z [true false 1 2 3 true false])))

(defn función-some-fn-15
  []
  (let [f (fn [x] (filterv boolean? x))
        g (fn [x] (filterv even? x))
        h (fn [x] (count x))
        z (some-fn h f g)]
    (z #{"hola" "buenas" true "false" false})))

(defn función-some-fn-16
  []
  (let [f (fn [x] (filter int? x))
        g (fn [x] (map-entry? x))
        h (fn [x] (take 5 x))
        z (some-fn h f g)]
    (z [1 1M 2 3 -1 -2 -3])))

(defn función-some-fn-17
  []
  (let [f (fn [x] (filterv double? x))
        g (fn [x] (hash-set x))
        h (fn [x] (string? (first x)))
        z (some-fn f h g)]
    (z '("abc" 2 "cde" 3.2 4.5))))


(defn función-some-fn-18
  []
  (let [f (fn [x] (filter char? x))
        g (fn [x] (count x))
        h (fn [x] (map dec x))
        z (some-fn f g h)]
    (z #{20 \a 23 \b 24 \c})))

(defn función-some-fn-19
  []
  (let [f (fn [x] (filterv float? x))
        g (fn [x] (odd? x))
        h (fn [x] (pos? (last x)))
        z (some-fn f h g)]
    (z [1 2 6 5.15 8 20.45])))

(defn función-some-fn-20
  []
  (let [f (fn [x] (filter nil? x))
        g (fn [x] (take 2 x))
        h (fn [x] (map-entry? x))
        z (some-fn f h g)]
    (z #{\a nil \b "hola" \c})))

(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)
;fin some-fn



